#ifndef DATACONTROLLER_H
#define DATACONTROLLER_H

#include <QObject>
#include <QDateTime>
#include <QFutureWatcher>
#include "datacache.h"
#include "shop.h"

class QNetworkReply;
class QNetworkAccessManager;

struct RequestContainer {
    enum RequestType {
        FetchShop,
        FetchReservation,
        FetchEmployee,
        FetchPrice
    };

    RequestContainer(const QString urlStr, const RequestType rqst, const QString idStr=QString())
        : url(urlStr), id(idStr), requestType(rqst) {
    }

    QString url;
    QString id;
    RequestType requestType;
};

class QGeoCoordinate;

class DataController : public QObject
{
    Q_OBJECT
public:
    explicit DataController(DataMemCache *cachePtr, QObject *parent = nullptr);
    Q_INVOKABLE void fetch(const QGeoCoordinate& center, const int distanceInMeters);
signals:
    void addNewItem(const QString id);
    void cancelDownload();
public slots:
    void requestFinished(QNetworkReply*);
    void shopsParsed();
    void reservationsParsed();
    void servicesParsed();
    void sendRequest(const QString &url);
private:
    void handleNextRequest();
    void fetchReservations(const QString employeeId);
    void fetchServices(const QString &serviceId, const QString &shopId);
    void fetchEmployee(const QString employeeId);
    static QList<Shop*>  parseShopRequestResult(DataController *thisPtr, const QByteArray result);
    static QList<ReservationContainer> parseReservations(QString &employeeId, const QByteArray result);
    static QList<ServiceContainer> parseServices(const QString &employeeId, const QByteArray &result);
    void parseEmployees(const QByteArray result);
    void parseShop(const QByteArray result);
    void parseReservations(const QByteArray result, const QString &requestID);
    void parseServices(const QByteArray result, const QString &requestID);

    DataMemCache *m_dataCache;
    QMutex m_mutex;
    QList<RequestContainer> m_requestQueue;
    QNetworkAccessManager *m_manager;
    QFutureWatcher<QList<Shop*>> *m_shopParserWatcher;
    QFutureWatcher<Reservations>*m_reservationParserWatcher;
    QFutureWatcher<Services>* m_servicesParserWatcher;
    bool m_requestPending;
};

#endif // DATACONTROLLER_H
