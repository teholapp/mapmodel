#ifndef WORKINGHOURSMODEL_H
#define WORKINGHOURSMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QDate>
#include <QMap>

class WorkingHours
{
public:
    QDateTime start() const {
        return m_start;
    }
    QDateTime end() const {
        return m_end;
    }

    void setStart(const QDateTime &s) {
        m_start = s;
    }
    void setEnd(const QDateTime &e) {
        m_end = e;
    }

    int durationInHours() {
        int duration = qRound(double(m_start.msecsTo(m_end)) / 1000.0 / 60.0 / 60.0);
        return duration;
    }

    inline bool operator==(const WorkingHours& other) const {
        if (start() == other.start() && end() == other.end()) return true;
        else return false;
    }

    static bool workingHoursLessThan(const WorkingHours &v1, const WorkingHours &v2)
    {
        return v1.start() < v2.start();
    }

private:
    QDateTime m_start;
    QDateTime m_end;
};

class WorkingHoursModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
public:
    explicit WorkingHoursModel(QObject *parent = 0);
    enum WorkingHourRoles {
        Type,
        Select,
        Enabled
    };

    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE QDateTime indexToDate(const int index) const;

    int rowCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

    QDate date() const;
    void setDate(const QDate& date);
    void indexToHoursAndMinutes(const int index, int& hours, int& mins) const;
    int hourAndMinutesToIndex(const int hour, const int min) const;
    void addWorkingHours(WorkingHours &workingHours);
    Q_INVOKABLE const QList<WorkingHours> combineWorkingHours() const;
    const QList<WorkingHours>& model() {
        return m_workingHours;
    };
    bool isWorkingNow();
signals:
    void dateChanged();


protected:
    int dateToIndex(const QDateTime& d);
    void generateModel();
private:
    QDate m_currentDay;
    QList<WorkingHours> m_workingHours;
    int m_interval;
    QVector<int> m_gridValues;
};

#endif // WORKINGHOURSMODEL_H
