#include <QLocale>
#include <QtDebug>
#include "timedelta.h"
#include "mapmodel.h"
#include "employee.h"

MapModel::MapModel(DataMemCache *dataCache, QObject *parent)
    :QAbstractListModel(parent)
    ,m_duration(60)
    ,m_dataCache(dataCache)
{
    connect(dataCache, SIGNAL(shopAdded(int, QString)), this, SLOT(shopAdded(int, QString)));
    connect(dataCache, SIGNAL(shopRemoved(int, QString)), this, SLOT(shopRemoved(int, QString)));
}

MapModel::~MapModel()
{
}

QHash<int, QByteArray> MapModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[ModelData] = "modelData";
    roles[Name] = "name";
    roles[Longitude] = "longitude";
    roles[Latitude] = "latitude";
    roles[Description] = "description";
    roles[Address] = "address";
    roles[Phone] = "phone";
    roles[Rating] = "rating";
    roles[Authenticated] = "authenticated";
    roles[Distance] = "distance";
    roles[NextFreeTime] = "freeslot";
    return roles;
}

int MapModel::duration() const
{
    return m_duration;
}

void MapModel::setDuration(const int duration)
{
    m_duration = duration;
    emit durationChanged();
}

int MapModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    return m_dataCache->count();
}

int MapModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

static QString distance(const Shop& shop, const QGeoCoordinate& currentPosition)
{
    QGeoCoordinate pos;
    pos.setLatitude(shop.latitude());
    pos.setLongitude(shop.longitude());
    qreal distance = pos.distanceTo(currentPosition);
    QString distanceStr = QString::number(distance / 1000, 'f', 1);
    distanceStr += "km";
    return distanceStr;
}


QVariant MapModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < m_dataCache->count()) {
        Shop *shop = m_dataCache->getShop(row);
        if (shop) {
            switch (role) {
            case ModelData:
                return QVariant::fromValue(shop);
            case Name:
                return shop->name();
            case Description:
                return shop->description();
            case Address:
                return shop->address();
            case Phone:
                return shop->phone();
            case Rating:
                return shop->rating();
            case Longitude:
                return shop->longitude();
            case Latitude:
                return shop->latitude();
            case Authenticated:
                return shop->authenticated();
            case Distance:
                return distance(*shop, currentPosition());
            case NextFreeTime:
                QDateTime now = QDateTime::currentDateTime();
                FreeSlot slot = getNextFreeSlot(shop->currentEmployee());
                if (slot.time <= now) {
                    return (tr("Heti vapaa"));
                } else {
                    QLocale locale;
                    return (locale.toString(slot.time, QLocale::ShortFormat));
                }
            }
        }
    }
    return QVariant();
}

FreeSlot MapModel::getNextFreeSlot(const QString &employee) const
{
    FreeSlot returnValue;
    Employee *employeePtr = m_dataCache->getEmployee(employee);
    if (employeePtr) {
        //Go through to the employee working hours and find the slot which fits to the duration
        for (const auto &hours : employeePtr->workingHours()) {
            TimeDelta delta = hours.end() - hours.start();
            if (delta.InMinutes() >= m_duration) {
                bool reserved = false;
                for (const auto &reservation : employeePtr->reservations()) {
                    if (reservation.startTime > hours.start() &&
                            reservation.startTime <= hours.end()) {
                        TimeDelta deltaBetweenStart = reservation.startTime - hours.start();
                        if (deltaBetweenStart.InMinutes() < m_duration) {
                            reserved = true;
                            break;
                        }
                    }
                    if (reservation.endTime >= hours.start() &&
                            reservation.endTime < hours.end()) {
                        TimeDelta deltaBetweenEnd = hours.end() - reservation.endTime;
                        if (deltaBetweenEnd.InMinutes() < m_duration) {
                            reserved = true;
                            break;
                        }
                    }
                }
                if (!reserved) {
                    returnValue.time = hours.start();
                    break;
                }
            }
        }
    }
    return returnValue;
}

QGeoCoordinate MapModel::currentPosition() const
{
    return m_currentPos;
}

void MapModel::setCurrentPosition(const double latitude, const double longitude)
{
    m_currentPos.setLatitude(latitude);
    m_currentPos.setLongitude(longitude);
}

void MapModel::setCurrentPosition(const QGeoCoordinate &currentPos)
{
    m_currentPos = currentPos;
}

void MapModel::setRating(const QString& id, double value)
{
    foreach (Shop *shop, m_dataCache->getShops()) {
        if (shop->id() == id) {
            shop->setRating(value);
        }
    }
}

const Shop *MapModel::get(int row)
{
    if (row < m_dataCache->getShops().count()) {
        Shop *item = m_dataCache->getShop(row);
        return item;
    }
    return nullptr;
}

void MapModel::shopAdded(const int index, const QString id)
{
    Q_UNUSED(id);
    beginInsertRows(QModelIndex(), index, index);
    endInsertRows();
}

void MapModel::shopRemoved(const int index, const QString id)
{
    Q_UNUSED(id);
    beginRemoveRows(QModelIndex(), index, index);
    endRemoveRows();
}
