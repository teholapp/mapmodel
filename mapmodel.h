#ifndef MAPMODEL_H
#define MAPMODEL_H

#include <QAbstractListModel>
#include <QGeoCoordinate>
#include <QDateTime>
#include "datacache.h"
#include "shop.h"

struct FreeSlot {
    QString id;
    QDateTime time;
};

class MapModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QGeoCoordinate currentPos READ currentPosition WRITE setCurrentPosition NOTIFY currentPosChanged)
    Q_PROPERTY(int duration READ duration WRITE setDuration NOTIFY durationChanged)
public:
    MapModel(DataMemCache *dataCache, QObject *parent=nullptr);
    ~MapModel() Q_DECL_OVERRIDE;
    Q_INVOKABLE const Shop *get(int row);
    Q_INVOKABLE void setCurrentPosition(const double latitude, const double longitude);

    enum MapRoles {
        ModelData = Qt::UserRole + 1,
        Name,
        Longitude,
        Latitude,
        Description,
        Address,
        Phone,
        Rating,
        Authenticated,
        Distance,
        NextFreeTime
    };

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QGeoCoordinate currentPosition() const;
    void setCurrentPosition(const QGeoCoordinate &currentPos);
    void setRating(const QString& id, double value);
    int duration() const;
    void setDuration(const int duration);
signals:
    void currentPosChanged();
    void durationChanged();
public slots:
    void shopAdded(const int index, const QString id);
    void shopRemoved(const int index, const QString id);
private:
    FreeSlot getNextFreeSlot(const QString &employee) const;
    QGeoCoordinate m_currentPos;
    int m_duration;
    QStringList m_services;
    DataMemCache *m_dataCache;
    friend class MapFilterProxyModel;    
};

#endif // MAPMODEL_H
