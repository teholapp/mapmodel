#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include "employee.h"
#include "shop.h"

const char *dateFormat = "yyyy-MM-dd hh:mm:ss";

void doParseShop(const QJsonObject &shop, Shop &shopItem)
{
    shopItem.setName(shop[QStringLiteral("marketing_name")].toString());
    shopItem.setPhone(shop[QStringLiteral("phone_number")].toString());
    shopItem.setId(shop[QStringLiteral("_id")].toString());
    shopItem.setDescription(shop[QStringLiteral("description")].toString());
    shopItem.setProperty("etag", shop[QStringLiteral("_etag")].toString());

    QJsonObject address = shop[QStringLiteral("address")].toObject();
    shopItem.setCity(address[QStringLiteral("city")].toString());
    shopItem.setAddress(address[QStringLiteral("street")].toString());
    shopItem.setPostalCode(address[QStringLiteral("postal_code")].toString());
    shopItem.setMobile(shop[QStringLiteral("moving")].toBool());
    shopItem.setAuthenticated(shop[QStringLiteral("authenticated")].toBool());
    shopItem.setRating(shop[QStringLiteral("average_rating")].toDouble());

    QJsonArray employees = shop[QStringLiteral("employees")].toArray();
    foreach (const QJsonValue & v, employees) {
           QString employee = v.toString();
           shopItem.addEmployee(employee);
    }

    QJsonObject location = shop[QStringLiteral("location")].toObject();
    QJsonArray coordinates = location[QStringLiteral("coordinates")].toArray();
    if (coordinates[0].isDouble())
        shopItem.setLongitude(coordinates[0].toDouble());
    if (coordinates[1].isDouble())
        shopItem.setLatitude(coordinates[1].toDouble());
}

Services doParseServices(const QString &employeeId, const QByteArray &result)
{
    Services services;
    services.employeeID = employeeId;
    QJsonDocument d = QJsonDocument::fromJson(result);
    QJsonArray items = d.object()[QStringLiteral("_items")].toArray();
    foreach (const QJsonValue & value, items) {
        ServiceContainer service;
        service.employeeId = employeeId;
        QJsonObject obj = value.toObject();
        service.price = obj["price"].toInt();
        QString serviceDescr = obj["description"].toString();
        QStringList titleAndService = serviceDescr.split("<br>");
        service.service = titleAndService.first();
        titleAndService.pop_front();
        if (titleAndService.count() > 0)
            service.description = titleAndService.first();
        service.duration = obj["duration_in_minutes"].toInt();
        service.id = obj["_id"].toInt();
        services.services.append(service);
    }
    return services;
}

static void doParseWorkingHours(const QJsonArray &workingHoursJson, Employee &employee)
{
    const QDateTime now = QDateTime::currentDateTime();
    for (const auto &value : workingHoursJson) {
        QJsonObject hourObject = value.toObject();
        QDateTime starttime = QDateTime::fromString(hourObject["start_datetime"].toString(), dateFormat);
        QDateTime endtime = QDateTime::fromString(hourObject["end_datetime"].toString(), dateFormat);
        if (endtime >= now) {
            WorkingHours workingHours;
            workingHours.setStart(starttime);
            workingHours.setEnd(endtime);
            employee.addWorkingHours(workingHours);
        }
    }
}

void doParseEmployee(const QJsonObject &employeeJson, Employee &employee)
{
    const QString employeeId = employeeJson[QStringLiteral("_id")].toString();
    const QString phone_number = employeeJson[QStringLiteral("phone_number")].toString();
    const QString first_name = employeeJson[QStringLiteral("first_name")].toString();
    const QString last_name = employeeJson[QStringLiteral("last_name")].toString();
    const QString email = employeeJson[QStringLiteral("email")].toString();
    const QString etag = employeeJson[QStringLiteral("_etag")].toString();
    employee.setId(employeeId);
    employee.setPhone(phone_number);
    employee.setFirstname(first_name);
    employee.setLastname(last_name);
    employee.setEmail(email);
    employee.setEtag(etag);
    QJsonArray workingHoursJson = employeeJson[QStringLiteral("working_hours")].toArray();
    doParseWorkingHours(workingHoursJson, employee);
}

Reservations doParseReservations(const QByteArray &result, const QString &employeeID)
{
    Reservations reservations;
    reservations.employeeID = employeeID;
    const QDateTime now = QDateTime::currentDateTime();
    QJsonDocument d = QJsonDocument::fromJson(result);
    QJsonArray items = d.object()[QStringLiteral("_items")].toArray();
    if (items.count() > 0) {
        foreach (const auto& value, items) {
            QJsonObject obj = value.toObject();
            QDateTime starttime = QDateTime::fromString(obj["start_datetime"].toString(), dateFormat);
            QDateTime endtime = QDateTime::fromString(obj["end_datetime"].toString(), dateFormat);
            //Skip reservations in the past
            if (endtime > now) {
                ReservationContainer reservationItem(starttime, endtime);
                reservations.reservations.append(reservationItem);
            }
        }
    }
    return reservations;
}

#endif
