#include <QDebug>
#include "datacache.h"
#include "employee.h"
#include "shop.h"

DataMemCache::DataMemCache(QObject *parent) : QObject(parent)
{

}

DataMemCache::~DataMemCache()
{
    qDeleteAll(m_shops);
    m_shops.clear();
}

bool DataMemCache::shopIndex(const QString& id, int &index)
{
    bool found = false;
    for (int i=0;i<m_shops.count(); i++) {
        if (m_shops.at(i)->id() == id) {
            index = i;
            found = true;
            break;
        }
    }
    return found;
}

void DataMemCache::removeIndex(int index)
{
    QString id;
    m_mutex.lock();
    Shop *removeme = m_shops.at(index);
    id = removeme->id();
    m_shops.removeOne(removeme);
    delete removeme;
    m_mutex.unlock();
    emit shopRemoved(index, id);
}

void DataMemCache::insertShop(int index, Shop *newShop)
{
    m_mutex.lock();
    if (index >= m_shops.count()) {
        m_shops.append(newShop);
        index = m_shops.count() - 1;
    } else
        m_shops.insert(index, newShop);
    m_mutex.unlock();
    emit shopAdded(index, newShop->id());
}

Shop * DataMemCache::getShop(const QString &id)
{
    for (Shop *shop : m_shops) {
        if (shop->id() == id) {
            return shop;
        }
    }
    return nullptr;
}

Shop *DataMemCache::getShop(int index)
{
    if (index < m_shops.count())
        return m_shops.at(index);
    return nullptr;
}

Employee * DataMemCache::getEmployee(const QString &employeeId)
{
    for (const Shop *shop : m_shops) {
        for (Employee* user : shop->employees()) {
            if (user && user->id() == employeeId) {
                return user;
            }
        }
    }
    return nullptr;
}

const QList<Shop *> &DataMemCache::getShops()
{
    return m_shops;
}

QVector<ReservationContainer> DataMemCache::getReservations(const QString &id, const QDate &begin, const QDate &end)
{
    QVector<ReservationContainer> ret;
    for (const auto *shop : m_shops) {
        if (shop->id() == id) {
            for (const ReservationContainer &reservation :
                shop->getEmployee(shop->currentEmployee())->reservations()) {
                if (reservation.startTime.date() >= begin &&
                        reservation.endTime.date() <= end) {
                    ret.append(reservation);
                }
            }
        }
    }
    return ret;
}

