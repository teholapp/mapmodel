#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFuture>
#include <QFutureWatcher>
#include <QThread>
#include <QGeoRectangle>
#include <QtConcurrent/QtConcurrentRun>
#include "datacontroller.h"
#include "jsonparser.h"

const char *url = "https://urlto";
const char *apiKey= "secretkey1234";

DataController::DataController(DataMemCache *cachePtr, QObject *parent) : QObject(parent)
  ,m_dataCache(cachePtr)
  ,m_manager(new QNetworkAccessManager(this))
  ,m_shopParserWatcher(nullptr)
  ,m_reservationParserWatcher(nullptr)
  ,m_servicesParserWatcher(nullptr)
  ,m_requestPending(false)

{
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(requestFinished(QNetworkReply*)));
}

void DataController::fetch(const QGeoCoordinate& center, const int distanceInMeters)
{
    QDateTime now = QDateTime::currentDateTime();
    QString dateStr = now.toString("yyyy-MM-dd%20hh:mm:ss");
    QString queryStr = QString("lon=%1&lat=%2&max_distance=%3&start_date=%4&duration=30").
            arg(center.longitude()).
            arg(center.latitude()).
            arg(QString::number(distanceInMeters)).
            arg(dateStr);
    RequestContainer request(QString(url) + "shops?" + queryStr,
                    RequestContainer::FetchShop);
    m_requestQueue.append(request);
    handleNextRequest();
}

void DataController::fetchReservations(const QString employeeId)
{
    const QString urlStr = QString(QString(url) + "reservations?where={\"employee\": \"%1\"}").arg(employeeId);
    RequestContainer request(urlStr, RequestContainer::FetchReservation, employeeId);
    m_requestQueue.append(request);
    handleNextRequest();
}

void DataController::fetchEmployee(const QString employeeId)
{
    const QString urlStr = QString(QString(url) + "employees?where={\"_id\": \"%1\"}").arg(employeeId);
    RequestContainer request(urlStr, RequestContainer::FetchEmployee, employeeId);
    m_requestQueue.append(request);
    handleNextRequest();
}

void DataController::fetchServices(const QString &serviceId, const QString &employeeId)
{
    const QString urlStr = QString(QString(url) + "service_types?where={\"_id\": \"%1\"}").arg(serviceId);
    RequestContainer request(urlStr, RequestContainer::FetchPrice, employeeId);
    m_requestQueue.append(request);
    handleNextRequest();
}

void DataController::sendRequest(const QString &requestStr)
{
    if (m_requestPending)
        return;
    m_requestPending = true;
#if defined(MAPMODELDEBUG)
    qDebug() << "SEND REQUEST: " << requestStr;
#endif
    QNetworkRequest request(requestStr);
    request.setRawHeader("X-Api-Key", apiKey);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply *networkReply = m_manager->get(request);
    QObject::connect(this, SIGNAL(cancelDownload()), networkReply, SLOT(abort()));
    connect(networkReply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
        [=](QNetworkReply::NetworkError code){
#if defined(MAPMODELDEBUG)
        qDebug() << "ERROR: " << code;
#else
        Q_UNUSED(code);
#endif
    m_requestPending = false;
    if(m_requestQueue.count() > 0)
        m_requestQueue.pop_front();
    handleNextRequest();});
}

void DataController::handleNextRequest()
{
    if (m_requestQueue.count() > 0) {
        QMetaObject::invokeMethod( this, "sendRequest", Qt::QueuedConnection,
                                   Q_ARG( QString, m_requestQueue.at(0).url ) );
    }
}

void DataController::requestFinished(QNetworkReply *reply)
{
    QByteArray responseData = reply->readAll();
#if defined(MAPMODELDEBUG)
    qDebug() << "response: " << responseData;
#endif
    if (m_requestQueue.count() > 0) {
        const RequestContainer request = m_requestQueue.first();
        m_requestQueue.pop_front();
        //Parse shops in separate thread
        switch (request.requestType) {
        case RequestContainer::FetchShop:
            parseShop(responseData);
            break;
        case RequestContainer::FetchReservation:
            parseReservations(responseData, request.id);
            break;
        case RequestContainer::FetchPrice:
            parseServices(responseData, request.id);
            break;
        case RequestContainer::FetchEmployee:
            parseEmployees(responseData);
            break;
        default:
            //Ignore other
            break;
        }
        m_requestPending = false;
        handleNextRequest();
    }
}

void DataController::parseShop(const QByteArray result)
{
    if (m_shopParserWatcher)
        m_shopParserWatcher->deleteLater();

    m_shopParserWatcher = new QFutureWatcher<QList<Shop *>>(this);
    connect(m_shopParserWatcher, SIGNAL(finished()), this, SLOT(shopsParsed()));
    QFuture<QList<Shop *>> future = QtConcurrent::run(DataController::parseShopRequestResult, this, result);
    m_shopParserWatcher->setFuture(future);
}

void DataController::parseReservations(const QByteArray result, const QString &requestID)
{
    if (m_reservationParserWatcher)
        m_reservationParserWatcher->deleteLater();

    m_reservationParserWatcher = new QFutureWatcher<Reservations>(this);
    connect(m_reservationParserWatcher, SIGNAL(finished()), this, SLOT(reservationsParsed()));
    QFuture<Reservations> future = QtConcurrent::run(doParseReservations, result, requestID);
    m_reservationParserWatcher->setFuture(future);
}

void DataController::parseServices(const QByteArray result, const QString &requestID)
{
    if (m_servicesParserWatcher)
        m_servicesParserWatcher->deleteLater();

    m_servicesParserWatcher = new QFutureWatcher<Services>(this);
    connect(m_servicesParserWatcher, SIGNAL(finished()), this, SLOT(servicesParsed()));
    QFuture<Services> future = QtConcurrent::run(doParseServices, requestID, result);
    m_servicesParserWatcher->setFuture(future);
}

void DataController::parseEmployees(const QByteArray result)
{
    QJsonDocument d = QJsonDocument::fromJson(result);
    const QJsonArray items = d.object()[QStringLiteral("_items")].toArray();
    if (items.count() > 0) {
        QJsonObject employeeJson = items.at(0).toObject();
        QString employeeId = employeeJson[QStringLiteral("_id")].toString();
        Employee *employee = m_dataCache->getEmployee(employeeId);
        if (employee) {
            doParseEmployee(employeeJson, *employee);
            if (employee->workingHours().count() > 0)
                fetchReservations(employee->id());

            //Fetch the services
            QJsonArray services = employeeJson[QStringLiteral("service_types")].toArray();
            foreach (const QJsonValue & value, services) {
                QString serviceID = value.toString();
                fetchServices(serviceID, employee->id());
            }
        } else {
#ifdef MAPMODELDEBUG
            qWarning() << "Parsing of the employee failed";
#endif
        }
    }
}

QList<Shop*> DataController::parseShopRequestResult(DataController *thisPtr, const QByteArray data)
{
    QJsonDocument d = QJsonDocument::fromJson(data);
    QJsonArray items = d.object()[QStringLiteral("_items")].toArray();
    QList<Shop*> shops;
    foreach (const auto &val, items) {
        QJsonObject shop = val.toObject();
        Shop *shopItem = new Shop();
        doParseShop(shop, *shopItem);
        shopItem->moveToThread(thisPtr->thread());
        shops.append(shopItem);
    }
    return shops;
}

void DataController::servicesParsed()
{
    const Services &services = m_servicesParserWatcher->result();
    Employee* employee = m_dataCache->getEmployee(services.employeeID);

    if (employee) {
        for (const auto &service : services.services) {
            employee->addService(service);
        }
    }
}

void DataController::reservationsParsed()
{
    const Reservations reservations = m_reservationParserWatcher->result();
    Employee* employee = m_dataCache->getEmployee(reservations.employeeID);
    if (employee) {
        for (const auto &reservation : reservations.reservations) {
            employee->addReservation(reservation);
        }
    }
}

void DataController::shopsParsed()
{
    QList<Shop*> shops = m_shopParserWatcher->result();
#if defined(MAPMODELDEBUG)
    qDebug() << "shopsParsed" << shops.count();
#endif
    if (shops.count() > 0) {
        //Remove existing shops
        for (auto *shop : shops) {
            int index = 0;
            bool isDuplicate = m_dataCache->shopIndex(shop->id(), index);
            if (isDuplicate) {
                m_dataCache->removeIndex(index);
            }
        }
        for (int i=0; i<shops.count(); i++) {
            const QString employee = shops.at(i)->currentEmployee();
            if (!employee.isEmpty()) {
                fetchEmployee(employee);
            }

            m_dataCache->insertShop(i, shops.at(i));
            emit addNewItem(shops.at(i)->id());
        }
    }
}


