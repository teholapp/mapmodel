#include <QDateTime>
#include <QDate>
#include <QDebug>
#include <QList>
#include "datacache.h"
#include "workinghours.h"

WorkingHoursModel::WorkingHoursModel(QObject *parent) : QAbstractListModel(parent)
  ,m_currentDay(QDateTime::currentDateTime().date()), m_interval(60)
{
    for (int i=0; i<rowCount(); i++) {
        m_gridValues.append(0);
    }
}

QHash<int, QByteArray> WorkingHoursModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Type] = "typeRole";
    roles[Select] = "select";
    roles[Enabled] = "enabled";
    return roles;
}

QDate WorkingHoursModel::date() const
{
    return m_currentDay;
}

void WorkingHoursModel::setDate(const QDate& date)
{
    m_currentDay = date;
    generateModel();
    emit dateChanged();
}

int WorkingHoursModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    return 24*7;
}

int WorkingHoursModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

void WorkingHoursModel::addWorkingHours(WorkingHours &workingHours)
{
    const int duration = workingHours.durationInHours();
    const int hour = 60*60;
    QDateTime start = workingHours.start();
    QDateTime end = start.addSecs(hour);
    for (int i=0; i<duration; i++) {
        WorkingHours hours;
        hours.setStart(start);
        hours.setEnd(end);

        if (!m_workingHours.contains(hours)) {
            m_workingHours.append(hours);
            //Insert grid value
            QDate monday = m_currentDay;
            const int day = monday.dayOfWeek();
            monday = monday.addDays((day-1)*-1);
            const QDate sunday = monday.addDays(6);

            if (hours.start().date() >= monday &&
                hours.start().date() <= sunday) {
                const int row = dateToIndex(hours.start());
                m_gridValues[row] = 1;
                emit dataChanged(index(row), index(row));
            }
        }
        start = start.addSecs(hour);
        end = end.addSecs(hour);
    }
    qSort(m_workingHours.begin(), m_workingHours.end(), WorkingHours::workingHoursLessThan);
}

const QList<WorkingHours> WorkingHoursModel::combineWorkingHours() const
{
    QList<WorkingHours> returnVal;
    WorkingHours newOne;
    QDate today = QDateTime::currentDateTime().date();
    for (const WorkingHours &work : m_workingHours) {
        //Skip old days
        if (work.start().date() < today) {
            continue;
        }
        if (!newOne.start().isValid()) {
            newOne.setStart(work.start());
            newOne.setEnd(work.end());
        }
        else if (work.start() == newOne.end()) {
            newOne.setEnd(work.end());
        } else {
            returnVal.append(newOne);
            newOne.setStart(work.start());
            newOne.setEnd(work.end());
        }
    }

    if (newOne.start().isValid() && newOne.end().isValid()) {
        returnVal.append(newOne);
    }

    return returnVal;
}

QVariant WorkingHoursModel::data(const QModelIndex &index, int role) const
{
    if (role == Type) {
        if (m_gridValues.at(index.row())) {
            return "working";
        }
        return "nonworking";
    } else if (role == Enabled) {
        return true;
        QDateTime startTime =  indexToDate(index.row());
        QDateTime now = QDateTime::currentDateTime();
        now = now.addSecs(-3600);
        if (startTime < now)
            return false;
        else
            return true;
    }
    return QVariant();
}

bool WorkingHoursModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Select) {
        QDateTime startTime =  indexToDate(index.row());
        QDateTime endTime = startTime.addSecs(m_interval * 60);

        if (value == "working") {
            //See if there's only one marking today
            int count = 0;
            WorkingHours item;

            if (m_workingHours.count() > 0) {
                WorkingHours &theOne = m_workingHours.first();
                for (WorkingHours &work : m_workingHours) {
                    if (work.start().date() == startTime.date()) {
                        theOne = work;
                        count++;
                    }
                }
                if (count == 1 && theOne.start() < startTime) {
                    startTime = theOne.start();
                    m_workingHours.removeOne(theOne);
                }
            }

            item.setStart(startTime);
            item.setEnd(endTime);
            addWorkingHours(item);
        } else {
            for (WorkingHours &work : m_workingHours) {
                if (work.start() == startTime &&
                         work.end() == endTime) {
                    m_gridValues[index.row()] = 0;
                    m_workingHours.removeOne(work);
                    break;
                }
            }
        }
    }
    qSort(m_workingHours.begin(), m_workingHours.end(), WorkingHours::workingHoursLessThan);
    emit dataChanged(index, index);
    return true;
}

void WorkingHoursModel::generateModel()
{
    QMap<int, bool> gridIndexes;
    //Get monday - sunday, assuming that week start from monday
    //TODO, american
    QDate monday = m_currentDay;
    int day = monday.dayOfWeek();
    monday = monday.addDays((day-1)*-1);
    QDate sunday = monday.addDays(6);

    for (const WorkingHours &work : m_workingHours) {

        if (work.start().date() >= monday &&
            work.start().date() <= sunday) {
            int index = dateToIndex(work.start());
            gridIndexes[index] = true;
        }
    }

    for (int i=0; i<m_gridValues.count(); i++) {
        int &value = m_gridValues[i];
        if (gridIndexes.contains(i)) {
            if (value == 0) {
                value = 1;
                emit dataChanged(index(i), index(i));
            }
        } else if(value == 1) {
            value = 0;
            emit dataChanged(index(i), index(i));
        }
    }
}

void WorkingHoursModel::indexToHoursAndMinutes(const int index, int& hours, int& mins) const
{
    int interval = 30;
    int steps = 60/interval;
    if (index == 0) {
        hours = 0;
        mins = (index % steps) * interval;
    }
    else {
        hours = index / steps;
        mins = (index % steps) * interval;
    }
}

int WorkingHoursModel::hourAndMinutesToIndex(const int hour, const int min) const
{
    int ret = hour * (60/m_interval);
    ret += min / m_interval;
    return ret;
}


int WorkingHoursModel::dateToIndex(const QDateTime& d)
{
    QDateTime datetime = d;
    QTime time = datetime.time();
    QDate date = datetime.date();

    int dayofweek = date.dayOfWeek();
    int steps = 60/m_interval;

    int dayIndex = (dayofweek-1) * (24*steps);
    int hourIndex = time.hour()*steps;
    int minIndex = time.minute() / m_interval;

    int ret = dayIndex + hourIndex + minIndex;
    return ret;
}

QDateTime WorkingHoursModel::indexToDate(const int index) const
{
    QDate now = m_currentDay;
    int dayofweek = now.dayOfWeek();
    now = now.addDays((dayofweek-1)*-1);

    int multipler = (60/m_interval);
    int day = index / (24 * multipler);
    now = now.addDays(day);
    int hour = (index - (day*24*multipler)) / multipler;
    int min = ((index - (day*24*multipler))%multipler)*m_interval;

    QDateTime ret;
    ret.setDate(now);
    QTime t;
    t.setHMS(hour, min, 0);
    ret.setTime(t);
    return ret;
}

bool WorkingHoursModel::isWorkingNow() {
    QDateTime now = QDateTime::currentDateTime();
    bool isWorking = false;
    for (const WorkingHours& hour: m_workingHours) {
        if (now.date() == hour.start().date() &&
                (now >= hour.start() && now <= hour.end())) {
            isWorking = true;
            break;
        }
    }
    return isWorking;
}
