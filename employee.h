#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <QObject>
#include "workinghours.h"

struct ReservationContainer
{
    ReservationContainer(const QDateTime& startT, const QDateTime& endT)
        : startTime(startT), endTime(endT) {

    }

    QDateTime startTime;
    QDateTime endTime;
};

struct ServiceContainer {
    QString employeeId;
    QString service;
    QString description;
    QString id;
    QString etag;
    int duration;
    int price;
};

struct Reservations {
    QString employeeID;
    QVector<ReservationContainer> reservations;
};

struct Services {
    QString employeeID;
    QVector<ServiceContainer> services;
};

static bool ReservationCompare(const ReservationContainer& i, const ReservationContainer& j)
{
    return i.startTime < j.endTime;
}

class Employee : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(QString firstname READ firstname WRITE setFirstname NOTIFY firstnameChanged)
    Q_PROPERTY(QString lastname READ lastname WRITE setLastname NOTIFY lastnameChanged)
    Q_PROPERTY(QString avatar READ avatar WRITE setAvatar NOTIFY avatarChanged)
    Q_PROPERTY(QString color READ color NOTIFY colorChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(QString etag READ etag WRITE setEtag NOTIFY etagChanged)
    Q_PROPERTY(QString password MEMBER m_password)
    Q_PROPERTY(bool currentUser READ currentUser WRITE setCurrentUser NOTIFY currentUserChanged)
    Q_PROPERTY(QString userType READ userType WRITE setUserType NOTIFY userTypeChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectionChanged)
public:
    explicit Employee(QObject *parent = 0) : QObject(parent) {
        m_color = "white";
        m_selected = true;
    }

    Employee(const Employee &other, QObject *parent=nullptr) : QObject(parent) {
        setId(other.id());
        setPhone(other.phone());
        setEmail(other.email());
        setUsername(other.username());
        setFirstname(other.firstname());
        setLastname(other.lastname());
        setAvatar(other.avatar());
        setColor(other.color());
        setEtag(other.etag());
        setCurrentUser(other.currentUser());
        setUserType(other.userType());
        for (const WorkingHours &wh : other.workingHours())
            addWorkingHours(wh);
        for (const ServiceContainer &service : other.services()) {
            m_services.append(service);
        }
        for (const ReservationContainer &reservation : other.reservations()) {
            m_reservations.append(reservation);
        }
    }

    QString id() const { return m_id; }
    QString username() const { return m_username; }
    QString firstname() const { return m_firstname; }
    QString lastname() const { return m_lastname; }
    QString avatar() const { return m_avatar; }
    QString color() const { return m_color; }
    QString email() const { return m_email; }
    QString phone() const { return m_phone; }
    bool selected() const { return m_selected; }
    QString etag() const { return m_etag; }

    void setUsername(const QString& username) { m_username = username; emit usernameChanged(); }
    void setFirstname(const QString& firstname) { m_firstname = firstname; emit firstnameChanged(); }
    void setLastname(const QString& lastname) { m_lastname = lastname; emit lastnameChanged(); }
    void setAvatar(const QString& avatar) { m_avatar = avatar; emit avatarChanged(); }
    void setColor(const QString& color) { m_color = color; emit colorChanged(); }
    void setEmail(const QString& email) { m_email = email; emit emailChanged(); }
    void setPhone(const QString& phone) { m_phone = phone; emit phoneChanged(); }
    void setSelected(bool selected) { m_selected = selected; emit selectionChanged(); }
    void setId(const QString& id) { m_id = id; emit idChanged(); }
    void setEtag(const QString& etag) { m_etag = etag; emit etagChanged();}

    bool currentUser() const {
        return m_currentUser;
    }

    void setCurrentUser(bool currentUser) {
        m_currentUser = currentUser;
    }

    QString userType() const {
        return m_userType;
    }

    void setUserType(const QString &type) {
        m_userType = type;
    }

    void addWorkingHours(const WorkingHours &hours)
    {
        m_workingHours.append(hours);
        qSort(m_workingHours.begin(), m_workingHours.end(), WorkingHours::workingHoursLessThan);
        emit workingHoursChanged();
    }

    const QList<WorkingHours>& workingHours() const
    {
        return m_workingHours;
    }

    void addReservation(const ReservationContainer &reservation)
    {
        m_reservations.append(reservation);
        qSort(m_reservations.begin(), m_reservations.end(), ReservationCompare);
    }

    const QList<ReservationContainer>& reservations() const
    {
        return m_reservations;
    }

    const QList<ServiceContainer>& services() const {
        return m_services;
    }
    void addService(const ServiceContainer &service) {
        m_services.append(service);
    }


signals:
    void usernameChanged();
    void firstnameChanged();
    void lastnameChanged();
    void avatarChanged();
    void colorChanged();
    void emailChanged();
    void phoneChanged();
    void idChanged();
    void selectionChanged();
    void etagChanged();
    void currentUserChanged();
    void userTypeChanged();
    void workingHoursChanged();

private:
    QString m_username;
    QString m_firstname;
    QString m_lastname;
    QString m_avatar;
    QString m_color;
    QString m_email;
    QString m_phone;
    bool m_selected;
    QString m_id;
    QString m_etag;
    QString m_password;
    bool m_currentUser;
    QString m_userType;
    QList<WorkingHours> m_workingHours;
    QList<ReservationContainer> m_reservations;
    QList<ServiceContainer> m_services;
};
#endif // EMPLOYEE_H
