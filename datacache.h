#ifndef DATACACHE_H
#define DATACACHE_H

#include <QObject>
#include <QMutex>
#include "shop.h"
#include "employee.h"

class MeetingItem;
class Employee;

class DataMemCache : public QObject
{
    Q_OBJECT
public:
    DataMemCache(QObject *parent = nullptr);
    ~DataMemCache();
    void insertShop(int index, Shop *newShop);
    bool shopIndex(const QString& id, int &index);
    void removeIndex(int index);
    Employee *getEmployee(const QString &employeeId);
    Shop *getShop(const QString &id);
    Shop *getShop(int index);
    const QList<Shop*>& getShops();
    QVector<ReservationContainer> getReservations(const QString& id,
                                                  const QDate& begin,
                                                  const QDate &end);
    int count() {
        return m_shops.count();
    }
signals:
    void shopAdded(const int index, const QString id);
    void shopRemoved(const int index, const QString id);
private:
    QList<Shop *> m_shops;
    QMutex m_mutex;
};

#endif // DATACACHE_H
