#include <QDebug>
#include "employee.h"
#include "shop.h"

Shop::Shop(QObject *parent) : QObject(parent), m_mobile(false), m_nextFreeTime(""), m_price(0)
{

}

Shop::Shop(const Shop& other, QObject *parent)
    :QObject(parent)
    ,m_id(other.id())
    ,m_name(other.name())
    ,m_description(other.description())
    ,m_rating(other.rating())
    ,m_address(other.m_address)
    ,m_latitude(other.m_latitude)
    ,m_longitude(other.m_longitude)
    ,m_phone(other.phone())
    ,m_city(other.city())
    ,m_postalCode(other.postalCode())
    ,m_authenticated(other.authenticated())
    ,m_etag(other.m_etag)
    ,m_mobile(other.mobile())
    ,m_price(other.price())
    ,m_nextFreeTime(other.nextFreeTime())

  {
  for (const Employee* user : other.m_employees) {
      m_employees.append(new Employee(*user));
  }
}

Shop::~Shop()
{
    qDeleteAll(m_employees);
    m_employees.clear();
}

QString Shop::id() const
{
    return m_id;
}

void Shop::setId(const QString &id)
{
    m_id = id;
}

QString Shop::name() const
{
    return m_name;
}

void Shop::setName(const QString &name)
{
    m_name = name;
}

QString Shop::description() const
{
    return m_description;
}

void Shop::setDescription(const QString &description)
{
    m_description = description;
}

double Shop::rating() const
{
    return m_rating;
}

void Shop::setRating(const double rating)
{
    m_rating = rating;
    emit ratingChanged();
}

QString Shop::address() const
{
    return m_address;
}

void Shop::setAddress(const QString &address)
{
    m_address = address;
}

double Shop::latitude() const
{
    return m_latitude;
}

void Shop::setLatitude(double latitude)
{
    m_latitude = latitude;
}

double Shop::longitude() const
{
    return m_longitude;
}

void Shop::setLongitude(double longitude)
{
    m_longitude = longitude;
}

QString Shop::phone() const
{
    return m_phone;
}

void Shop::setPhone(const QString &phone)
{
    m_phone = phone;
}

QString Shop::city() const
{
    return m_city;
}

void Shop::setCity(const QString &city)
{
    m_city = city;
}

QString Shop::postalCode() const
{
    return m_postalCode;
}

void Shop::setPostalCode(const QString &postalCode)
{
    m_postalCode = postalCode;
}

bool Shop::authenticated() const
{
    return m_authenticated;
}

void Shop::setAuthenticated(bool authenticated)
{
    m_authenticated = authenticated;
}

void Shop::addEmployee(const QString& employee)
{
    bool found = false;
    for (Employee *user : m_employees) {
        if (user->id() == employee) {
            found = true;
            break;
        }
    }
    if (!found) {
        Employee *newUser = new Employee();
        newUser->setId(employee);
        m_employees.append(newUser);
    }
}

void Shop::setCurrentEmployee(const QString& id)
{
    //TODO
}

QString Shop::currentEmployee() const
{
    if (m_employees.length() > 0) {
        return m_employees.at(0)->id();
    }
    return QString();
}

Employee *Shop::getEmployee(const QString& id) const
{
    for (Employee *user : m_employees) {
        if (user->id() == id) {
            return user;
        }
    }
    return nullptr;
}

bool Shop::mobile() const
{
    return m_mobile;
}

void Shop::setMobile(bool mobile)
{
    m_mobile = mobile;
    emit mobileChanged();
}


QString Shop::nextFreeTime() const
{
    return m_nextFreeTime;
}

void Shop::setNextFreeTime(const QString &value)
{
    m_nextFreeTime = value;
    emit nextFreeTimeChanged();
}

int Shop::price() const
{
    return m_price;
}

void Shop::setPrice(const int price)
{
    m_price = price;
    emit priceChanged();
}

QString Shop::getEtag() const
{
    return m_etag;
}

void Shop::setEtag(const QString &etag)
{
    m_etag = etag;
    emit etagChanged();
}

