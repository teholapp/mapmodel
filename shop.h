#ifndef SHOP_H
#define SHOP_H

#include <QObject>
#include <QVector>
#include <QTime>

class Employee;

class Shop : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(double rating READ rating WRITE setRating NOTIFY ratingChanged)
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(QString city READ city WRITE setCity NOTIFY cityChanged)
    Q_PROPERTY(QString postalCode READ postalCode WRITE setPostalCode NOTIFY postalCodeChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)
    Q_PROPERTY(bool authenticated READ authenticated WRITE setAuthenticated NOTIFY authenticatedChanged)
    Q_PROPERTY(QString currentEmployee READ currentEmployee WRITE setCurrentEmployee NOTIFY currentEmployeeChanged)
    Q_PROPERTY(QString etag READ getEtag WRITE setEtag NOTIFY etagChanged)
    Q_PROPERTY(bool mobile READ mobile WRITE setMobile NOTIFY mobileChanged)
    Q_PROPERTY(QString nextFreeTime READ nextFreeTime NOTIFY nextFreeTimeChanged)
    Q_PROPERTY(int price READ price WRITE setPrice NOTIFY priceChanged)
public:
    Shop(QObject *parent=nullptr);
    Shop(const Shop& other, QObject *parent=nullptr);
    virtual ~Shop();
    QString id() const;
    void setId(const QString &id);

    QString name() const;
    void setName(const QString &name);

    QString description() const;
    void setDescription(const QString &description);

    double rating() const;
    void setRating(const double rating);

    QString address() const;
    void setAddress(const QString &address);

    double latitude() const;
    void setLatitude(double latitude);

    double longitude() const;
    void setLongitude(double longitude);

    QString phone() const;
    void setPhone(const QString &phone);

    QString city() const;
    void setCity(const QString &city);

    QString postalCode() const;
    void setPostalCode(const QString &postalCode);

    bool authenticated() const;
    void setAuthenticated(bool authenticated);
    QString currentEmployee() const;
    void setCurrentEmployee(const QString& id);
    void addEmployee(const QString& employee);
    Employee *getEmployee(const QString& id) const;
    const QList<Employee*>& employees() const {
        return m_employees;
    }
    bool mobile() const;
    void setMobile(bool mobile);

    QString nextFreeTime() const;
    void setNextFreeTime(const QString &value);
    int price() const;
    void setPrice(const int price);
    QString getEtag() const;
    void setEtag(const QString &etag);

signals:
    void postalCodeChanged();
    void cityChanged();
    void phoneChanged();
    void longitudeChanged();
    void latitudeChanged();
    void addressChanged();
    void idChanged();
    void descriptionChanged();
    void nameChanged();
    void ratingChanged();
    void authenticatedChanged();
    void currentEmployeeChanged();
    void etagChanged();
    void mobileChanged();
    void nextFreeTimeChanged();
    void priceChanged();
private:
    QString m_id;
    QString m_name;
    QString m_description;
    double m_rating;
    QString m_address;
    double m_latitude;
    double m_longitude;
    QString m_phone;
    QString m_city;
    QString m_postalCode;
    bool m_authenticated;
    QString m_etag;
    bool m_mobile;
    int m_price;
    QString m_nextFreeTime;
    QList<Employee *> m_employees;
};


#endif // SHOP_H
